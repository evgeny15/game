class CreatePlayers < ActiveRecord::Migration[5.1]
  def change
    create_table :players do |t|
      t.string :nickname
      t.integer :rank
      t.integer :charisma
      t.integer :wisdom

      t.timestamps
    end
  end
end
